use core::arch::x86_64::{CpuidResult, __cpuid_count};
use core::option;
use core::mem::MaybeUninit;
use x86_64::registers::model_specific::Msr;

pub fn is_system_compatible() -> Option<()> {
    // https://en.wikipedia.org/wiki/CPUID#EAX=0:_Highest_Function_Parameter_and_Manufacturer_ID
    // LE format, check if AMD processor 
    const CPUID_SIGNATURE_AUTHENTIC_AMD_EBX : &[u8;4] = b"htuA";
    const CPUID_SIGNATURE_AUTHENTIC_AMD_EDX : &[u8;4] = b"itne";
    const CPUID_SIGNATURE_AUTHENTIC_AMD_ECX : &[u8;4] = b"DMAc";
    let cpuid_result : MaybeUninit<CpuidResult> = unsafe { MaybeUninit::new(__cpuid_count(0, 0)) }; 
    if unsafe  { (*cpuid_result.as_ptr()).ebx  != u32::from_le_bytes(*CPUID_SIGNATURE_AUTHENTIC_AMD_EBX) } || 
        unsafe { (*cpuid_result.as_ptr()).edx  != u32::from_le_bytes(*CPUID_SIGNATURE_AUTHENTIC_AMD_EDX) } ||
        unsafe { (*cpuid_result.as_ptr()).ecx  != u32::from_le_bytes(*CPUID_SIGNATURE_AUTHENTIC_AMD_ECX) } {
            info!("[-] This is not an AMD CPU...");
            return None; 
        }
    
    // https://en.wikipedia.org/wiki/CPUID#EAX=80000001h:_Extended_Processor_Info_and_Feature_Bits
    // check the third bit on svm feature in ECX, must be set
    let cpuid_result : MaybeUninit<CpuidResult> = unsafe { MaybeUninit::new(__cpuid_count(0x80000001, 0)) }; 
    if unsafe { ((*cpuid_result.as_ptr()).ecx & ( 1 << 2)) == 0 } {
        info!("[-] AMD-V not supported...");
        return None;
    }

    // Developer Manual Volume 2, page 533: Enabling Nested Paging
    // EDX, NP bit must be set
    let cpuid_result : MaybeUninit<CpuidResult> = unsafe { MaybeUninit::new(__cpuid_count(0x8000000a, 0)) }; 
    if unsafe { ((*cpuid_result.as_ptr()).edx & 1) == 0 } {
        info!("[-] Nested Paging not supported...");
        return None;
    }

    // Check if SVM can be enabled requires checking the VM_CR MSR  (C001_0114h) Bit4=SVMDIS must be unset, so that  
    // EFER.SVME (Secure Virtual Machine Enable) is set; thus enabling SVM is possible. 
    let vm_cr_msr: MaybeUninit<u64> = unsafe { MaybeUninit::new(Msr::new(0xc0010114).read()) };
    if unsafe {((*vm_cr_msr.as_ptr()) & (1 << 3)) != 0} {
        info!("[-] AMD-V cannot be enabled...");
        return None;
    }

    // Check x2APIC, MSR 0000_001Bh bit 10 EXTD (X2APIC Mode Enable) must be unset and bit 11 APIC Enable (xAPIC mode)  must be set
    let local_apic_msr: MaybeUninit<u64> = unsafe { MaybeUninit::new(Msr::new(0x0000001b).read()) };
    if unsafe {((*local_apic_msr.as_ptr()) & (1 << 10)) == 0} ||
        unsafe {((*local_apic_msr.as_ptr()) & (1 << 9)) != 0}  {
        info!("[-] x2APIC is not supported or APIC not enabled...");
        return None;
    }
    
    // Check if PAT (Page-Attribute Table) is default by looking into MSR 0x00000277, Here we ensure that the memory type is set to Write-back
    // which means PA0 = 06h
    let pat_msr : MaybeUninit<u64> = unsafe { MaybeUninit::new(Msr::new(0x00000277).read())};
    if unsafe {*pat_msr.as_ptr() != 6} {
        info!("[-] Memory is not set for Write-Back...");
        return None;
    }
 
    Some(())  
}